
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  *
 * dismain.c - The main disassembler routine.  This handles a single    *
 *      pass for the disassembler routine. For each command, it passes  *
 *      off the job of disassembling that command.                      *
 *                                                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  *
 *                                                                      *
 * Copyright (c) 2017 David Breeding                                    *
 *                                                                      *
 * This file is part of osk-disasm.                                     *
 *                                                                      *
 * osk-disasm is free software: you can redistribute it and/or modify   *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * osk-disasm is distributed in the hope that it will be useful,        *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * (see the file "COPYING") along with osk-disasm.  If not,             *
 * see <http://www.gnu.org/licenses/>.                                  *
 *                                                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define _MAIN_  /* We will define all variables in one header file, then
                   define them extern from all other files */

#include <string.h>
#include <ctype.h>
#ifdef _OSK
#   include <types.h>
#else
#   include <sys/types.h>
#endif
#include "disglobs.h"
#include "textdef.h"
#include "modtypes.h"
#include "proto.h"
int error;

void reflst();
#ifdef _WIN32
#   define strcasecmp _stricmp
#endif

#ifdef _OSK
#   define strcasecmp strcmp
#endif

/* Some variables that are only used in one or two modules */
int LblFilz;              /* Count of Label files specified     */
char *LblFNam[MAX_LBFIL]; /* Pointers to the path names for the files */

/*static int HdrLen;*/
int CodeEnd;

extern struct databndaries *dbounds;
extern char realcmd[], pseudcmd[];
#ifdef __STDC__
    extern void printXtraBytes (char *);
#else
    extern void printXtraBytes ();
#endif

static int get_modhead();
static int get_asmcmd(
#ifdef __STDC__
    void
#endif
);

#if 0
/*
 * list_print() - Handle printouts of lines of code.  Prints to the listing
 *        and prints to asm code file if specified
 */

static void
#ifdef __STDC__
list_print (register CMD_ITMS *ci, register short ent, char register *lblnam)
#else
list_print (ci, ent, lblnam)
    register CMD_ITMS *ci;
    register int ent;
    register char *lblnam;
#endif
{
    register char *ListFmt = "%05x %04x %10.10s %s %s\n";
    register char *CmntFmt = "%05x %04x %10.10s %s %s %s\n";
    register char *lb;

    lb = (lblnam ? lblnam : lblstr('L', CmdEnt));

    /* When ready, need to provide for option whether to print or not */
    if (ci->comment)
    {
        printf(CmntFmt, ent & 0xffff, ci->cmd_wrd, lb,
                        ci->mnem, ci->opcode, ci->comment);
    }
    else
    {
        printf(ListFmt, ent & 0xffff, ci->cmd_wrd, lb,
                        ci->mnem, ci->opcode);
    }
    /* Provide for printing to asm src listing */

}
#endif

static char *DrvrJmps[] = {
    "Init", "Read", "Write", "GetStat", "SetStat", "Term", "Except"
};

static char *FmanJmps[] = {"Create", "Open", "Makdir", "Chgdir", "Delete", "Seek",
        "Read", "Write", "Readln", "Writeln", "Getstat", "Setstat", "Close"};

/* ***********************
 * get_drvr_jmps()
 *    Read the Driver initialization table and set up
 *    label names.
 */

static void
#ifdef __STDC__
get_drvr_jmps(register int mty)
#else
get_drvr_jmps(mty)
    register int mty;
#endif
{
    register char **pt;
    register short jmpdst;
    register int jmpbegin = ftell (ModFP);
    register char typ;
    register short pos = jmpbegin;
    register char **ptend;
    char boundstr[50];

    if (mty == MT_DEVDRVR)
    {
        pt = DrvrJmps;
        ptend = &pt[7];
    }
    else
    {
        pt = FmanJmps;
        ptend = &pt[13];
    }

    while (pt < ptend)
    {
        jmpdst = fread_w(ModFP);

        if (jmpdst)
        {
            addlbl ('L', jmpdst + (mty == MT_FILEMAN ? jmpbegin : 0), *pt);
            typ = 'L';
        }
        else
        {
            typ = '&';
        }

        if (mty == MT_FILEMAN)
        {
            sprintf (boundstr, "W %c (- L %x) %x", typ, jmpbegin, pos);
        }
        else {
            sprintf(boundstr, "W %c %x", typ, pos);
        }

        setupbounds (boundstr);
        pos += 2;
        ++pt;
    }

}

/* ****************************************************** *
 * Get the module header.  We will do this only in Pass 1 *
 * ****************************************************** */

static int
get_modhead()
{
    /* Get standard (common to all modules) header fields */

    M_ID = fread_w(ModFP) & 0xffff;

    switch (M_ID)
    {
    case 0x4afc:
        M_SysRev = fread_w(ModFP);
        M_Size = fread_l( ModFP);
        M_Owner = fread_l(ModFP);
        M_Name = fread_l(ModFP);
        M_Accs = fread_w(ModFP);
        M_Type = fread_b(ModFP);
        M_Lang = fread_b(ModFP);
        M_Attr = fread_b(ModFP);
        M_Revs = fread_b(ModFP);
        M_Edit = fread_w(ModFP);
        M_Usage = fread_l(ModFP);
        M_Symbol = fread_l(ModFP);

        /* We have 14 bytes that are not used */
        if (fseek(ModFP, 14, SEEK_CUR) != 0)
        {
            errexit("Cannot Seek to file location");
        }

        M_Parity = fread_w(ModFP);

        /* Now get any further Mod-type specific headers */

        switch (M_Type)
        {
        case MT_PROGRAM:
        case MT_SYSTEM:
        case MT_TRAPLIB:
        case MT_FILEMAN:
        case MT_DEVDRVR:
            M_Exec = fread_l(ModFP);
            
            /* Add label for Exception Handler, if applicable */
            /* Only for specific Module Types??? */
            if ((M_Except = fread_l(ModFP)))
            {
                addlbl('L', M_Except, "");
            }

            /* Add btext */
            /* applicable for only specific Moule Types??? */
            addlbl ('L', 0, "btext");

            HdrEnd = ftell(ModFP); /* We'll keep changing it if necessary */

            if ((M_Type != MT_SYSTEM) && (M_Type != MT_FILEMAN))
            {
                M_Mem = fread_l(ModFP);

                if (M_Type != MT_DEVDRVR)
                {
                    M_Stack = fread_l(ModFP);
                    M_IData = fread_l(ModFP);
                    M_IRefs = fread_l( ModFP);

                    if (M_Type == MT_TRAPLIB)
                    {
                        M_Init = fread_l(ModFP);
                        M_Term = fread_l(ModFP);
                    }
                }

                HdrEnd = ftell(ModFP);  /* The final change.. */
            }

            if ((M_Type == MT_DEVDRVR) || (M_Type == MT_FILEMAN))
            {
                fseek (ModFP, M_Exec, SEEK_SET);
                get_drvr_jmps (M_Type);
            }

           if (M_IData)
            {
                fseek (ModFP, M_IData, SEEK_SET);
                IDataBegin = fread_l (ModFP);
                IDataCount = fread_l (ModFP);
                /* Insure we have an entry for the first Initialized Data */
                addlbl ('D', IDataBegin, "");
            }

           CodeEnd = M_Size - 4;
        }

        fseek (ModFP, 0, SEEK_END);

        if (ftell(ModFP) < M_Size)
        {
            errexit ("\n*** ERROR!  File size < Module Size... Aborting...! ***\n");
        }

        break;
    case 0xdead:
        getRofHdr(ModFP);
        /*errexit("Disassembly of ROF files is not yet implemented.");*/
        break;
    default:
        errexit("Unknown module type");
    }

    return 0;
}

/*
 * modnam_find() - search a modna struct for the desired value
 * Returns: On Success: Pointer to the desired value
 *          On Failure: NULL
 */

struct modnam *
#ifdef __STDC__
modnam_find (register struct modnam *pt, register int desired)
#else
modnam_find (pt, desired)
    register struct modnam *pt;
    register int desired;
#endif
{
    while ((pt->val) && (pt->val != desired))
    {
        ++pt;
    }

    return (pt->val ? pt : NULL);
}

/* ******************************************************************** *
 * RdLblFile() - Reads a label file and stores label values into label  *
 *      tree if value (or address) is present in tree                   *
 *      Path to file has already been opened and is stored in "inpath"  *
 * ******************************************************************** */

static void
#ifdef __STDC__
RdLblFile (register FILE *inpath)
#else
RdLblFile (inpath)
    register FILE *inpath;
#endif
{
    char labelname[30],
         eq[10],
         strval[15],
         clas;
    register char  *lbegin;
    int address;
    register LBLDEF *nl;

    while ( ! feof (inpath))
    {
        char rdbuf[81];

        fgets (rdbuf, 80, inpath);

        if ( ! (lbegin = skipblank (rdbuf)) || (*lbegin == '*'))
        {
            continue;
        }

        if (sscanf (rdbuf, "%s %s %s %c", labelname, eq, strval, &clas) == 4)
        {
#ifdef _OSK
              register int c;
#endif
            clas = toupper (clas);

#ifdef _OSK
            for (c = 0; c < 3; c++)
            {
                eq[c] = _tolower(eq[c]);
            }
#endif
            if ( ! strcasecmp (eq, "equ"))
            {
                /* Store address in proper place */

                if (strval[0] == '$')   /* if represented in hex */
                {
                    sscanf (&strval[1], "%x", &address);
                }
                else
                {
                    address = atoi (strval);
                }

                nl = findlbl (clas, address);

                if (nl)
                {
                    strncpy (nl->sname, labelname, sizeof(nl->sname) - 1);
                    nl->sname[sizeof(nl->sname)] = '\0';
                    nl->stdname = 1;
                }
            }
        }
    }
}

/* ******************************************************** *
 * GetLabels() - Set up all label definitions               *
 *      Reads in all default label files and then reads     *
 *      any files specified by the '-s' option.             *
 * ******************************************************** */

static void
GetLabels ()                    /* Read the labelfiles */
{
    register u_char x;

    /* First, get built-in ascii definitions.  Later, if desired,
     * they can be redefined */

    /*for (x = 0; x < 33; x++)
    {
        if ((nl = FindLbl (ListRoot ('^'), x)))
        {
            strcpy (nl->sname, CtrlCod[x]);
        }
    }

    if ((nl = FindLbl (ListRoot ('^'), 0x7f)))
    {
        strcpy (nl->sname, "del");
    }*/

    /* Next read in the Standard systems names file */

    /*if ((OSType == OS_9) || (OSType == OS_Moto))
    {
        tmpnam = build_path ("sysnames");

        if ( ! (inpath = fopen (tmpnam, "rb")))
            fprintf (stderr, "Error in opening Sysnames file..%s\n",
                              filename);
        else
        {
            RdLblFile ();
            fclose (inpath);
        }
    }*/

    /* Now read in label files specified on the command line */

    for (x = 0; x < LblFilz; x++)
    {
        register FILE *inpath;

        if ((inpath = build_path (LblFNam[x], BINREAD)))
        {
            RdLblFile (inpath);
            fclose (inpath);
        }
        else
        {
            fprintf (stderr, "ERROR! cannot open Label File %s for read\n",
                     LblFNam[x]);
        }
    }
}

/*
 * dopass() - Do a complete single pass through the module.
 *      The first pass establishes the addresses of necessary labels
 *      On the second pass, the desired label names have been inserted
 *      and printing of the listing and writing of the source file is
 *      done, if either or both is desired.
 */

int
dopass()
{
    if (Pass == 1)
    {
        if (!(ModFP = fopen(ModFile, BINREAD)))
        {
            errexit("Cannot open Module file for read");
        }

        PCPos = 0;
        get_modhead();
        PCPos = ftell(ModFP);
        /*process_label(&Instruction, 'L', M_Exec);*/
        addlbl ('L', M_Exec, NULL);
        
        /* Set Default Addressing Modes according to Module Type */
        switch (M_Type)
        {
        case MT_PROGRAM:
            strcpy(DfltLbls, "&&&&&&D&&&&L");
            break;
        case MT_DEVDRVR:
            /*  Init/Term:
                 (a1)=Device Dscr
                Read/Write,Get/SetStat:
                 (a1)=Path Dscr, (a5)=Caller's Register Stack
                All:
                 (a1)=Path Dscr
                 (a2)=Static Storage, (a4)=Process Dscr, (a6)=System Globals

               (a1) & (a5) default to Read/Write, etc.  For Init/Term, specify
               AModes for these with Boundary Defs*/
            strcpy (DfltLbls, "&ZD&PG&&&&&L");
            break;
        }

        GetIRefs();
        do_cmd_file();

        setROFPass();
    }
    else   /* Do Pass 2 Setup */
    {
        /*parsetree ('A');*/
        GetLabels();

        /* We do this here so that we can rename labels
         * to proper names defined in the ROF file
         */
        if (IsROF)
        {
            setROFPass();
            RofLoadInitData();
        }

        WrtEquates (1);
        WrtEquates (0);
        /*showem();*/
        CmdEnt = 0;

        if (IsROF)
        {
            ROFPsect(&ROFHd);
            ROFDataPrint();
        }
        else
        {
            PrintPsect();
            OS9DataPrint();
        }
    }

    /* NOTE: This is just a temporary kludge The begin _SHOULD_ be
             the first byte past the end of the header, but until
             we get bounds working, we'll do  this. */
    if (fseek(ModFP, HdrEnd, SEEK_SET) != 0)
    {
        errexit("FIle Seek Error");
    }

    if (IsROF)
    {
        PCPos = 0;
    }
    else
    {
        PCPos = HdrEnd;
    }


    while (PCPos < CodeEnd)
    {
        struct databndaries *bp;

        memset(&Instruction, 0, sizeof(Instruction));
        CmdEnt = PCPos;

        /* NOTE: The 6809 version did an "if" and it apparently worked,
         *     but we had to do this to get it to work with consecutive bounds.
         */
        while ((bp = ClasHere (dbounds, PCPos)))
        {
            NsrtBnds (bp);
            CmdEnt = PCPos;
        }

        /* This does not work... it stops ALL printout, and as is,
           causes the program to hang */
        if (CmdEnt >= CodeEnd) return 0;
       
        if (get_asmcmd())
        {
            if (Pass == 2)
            {
                /*PrintComment('L', CmdEnt, PCPos);*/
                Instruction.comment = get_apcomment ('L', CmdEnt);
                /*list_print (&Instruction, CmdEnt, NULL);*/
                PrintLine (pseudcmd, &Instruction, 'L', CmdEnt, PCPos);

                if (PrintAllCode && Instruction.wcount)
                {
                    u_short count = Instruction.wcount;
                    char codbuf[50];
                    u_char wpos = 0;
                    /*printf("%6s", "");*/
                    codbuf[0] = '\0';

                    while (count)
                    {
                        char tmpcod[10];

                        sprintf (tmpcod, "%04x ", Instruction.code[wpos++]);
                        strcat (codbuf, tmpcod);
                        --count;
/*                        if (count > 1)
                        {
                            PrintAllCodLine(Instruction.code[wpos],
                                    Instruction.code[wpos + 1]);
                            count -= 2;
                            wpos += 2;
                        }
                        else
                        {
                            PrintAllCodL1(Instruction.code[wpos]);
                            --count;
                            ++wpos;
                        }*/
                       /*printf("%04x ", Instruction.code[count] & 0xffff);*/
                    }

                    printXtraBytes (codbuf);
                }
            }
        }
        else
        {
            if (Pass == 2)
            {
                strcpy (Instruction.mnem, "dc.w");
                sprintf (Instruction.opcode, "$%x",
                        Instruction.cmd_wrd & 0xffff);
                PrintLine (pseudcmd, &Instruction, 'L', CmdEnt, PCPos);
                CmdEnt = PCPos;
                /*list_print (&Instruction, CmdEnt, NULL);*/
            }
        }
    }

    if (Pass == 2)
    {
        WrtEnds();
    }

    /*reflst();*/
    return 0;
}

extern struct rof_extrn *refs_code;
int showem()
{
    /*register char c = '_';*/
    register struct rof_extrn *rf = refs_code;
    /*register LBLCLAS *l = labelclass(c);*/

    if (!rf)
        fprintf(stderr, "No Code refs found!\n");
    while (rf)
    {
        fprintf (stderr, "%04x: -> (%03x '%c') \"%-14s\" (%s)\n", rf->Ofst, rf->Type,
            rf->dstClass ? rf->dstClass : ' ', rf->Extrn ? rf->EName.nam : rf->EName.lbl->sname,
            rf->Extrn ? "Extern" : "Local");
        rf = rf->ENext;
    }

    return 0;
}

static CMD_ITMS *
#ifdef __STDC__
initcmditems (register CMD_ITMS *ci)
#else
initcmditems (ci)
    register CMD_ITMS *ci;
#endif
{
    ci->mnem[0] = 0;
    ci->wcount = 0;
    ci->opcode[0] ='\0';
    ci->comment = NULL;
    return ci;
}

/*
 * noimplemented() - A dummy function which simply returns NULL for instructions
 *       that do not yet have handler functions
 */

int
#ifdef __STDC__
notimplemented(register CMD_ITMS *ci, register int tblno, register OPSTRUCTURE *op)
#else
notimplemented (ci, tblno, op)
    register CMD_ITMS *ci;
    register int tblno;
    register OPSTRUCTURE *op;
#endif
{
    return 0;
}

/*extern OPSTRUCTURE *instr00,*instr01,*instr04,*instr05,*instr06,
       *instr07,*instr08,*instr09,*instr11,*instr12,*instr13,*instr14;*/
OPSTRUCTURE *opmains[] =
{
    instr00,
    instr01,
    instr02,    /* Repeat 3 times for 3 move sizes */
    instr03,
    instr04,
    instr05,
    instr06,
    instr07,
    instr08,
    instr09,
    NULL,       /* No instr 010 */
    instr11,
    instr12,
    instr13,
    instr14,
    NULL
};

static int
get_asmcmd()
{
    /*extern OPSTRUCTURE syntax1[];
    extern COPROCSTRUCTURE syntax2[];*/
    register OPSTRUCTURE *optbl;

    register u_short opword;
    register u_short j;
    /*int size;*/


    /*size = 0;*/
    Instruction.wcount = 0;
    opword = getnext_w (&Instruction);
    /* Make adjustments for this being the command word */
    Instruction.cmd_wrd = Instruction.code[0] & 0xffff;
    Instruction.wcount = 0; /* Set it back again */

    /* This may be temporary.  Opword %1111xxxxxxxxxxxx is available
     * for 68030-68040 CPU's, but we are not yet making this available
     */
    if ((opword & 0xf000) == 0xf000)
        return 0;
    
    if (!(optbl = opmains[(opword >> 12) & 0x0f]))
    {
        return 0;
    }

    for (j = 0; j <= MAXINST; j++)
    {
        OPSTRUCTURE *curop = &optbl[j];

        error = FALSE;

        if (!(curop->name))
            break;

        curop = tablematch (opword, curop);

        /* The table must be organized such that the "cpulvl" field
         * is sorted in ascending order.  Therefore, if a "cpulvl"
         * higher than "cpu" is encountered we can abort the search.
         */
        if (curop->cpulvl > cpu)
        {
            return 0;
        }

        if (!error)
        {
            if (curop->opfunc(&Instruction, curop->id, curop))
            {
                return 1;
            }
            else
            {
                if (ftell(ModFP) != RealEnt() + 2)
                {
                    fseek(ModFP, RealEnt() + 2, SEEK_SET);
                    PCPos = CmdEnt + 2;
                    initcmditems(&Instruction);
                }
            }
        }
    }

#if (DEVICE==68040 || COPROCESSOR==TRUE)
    for (h = 3; h <= MAXCOPROCINST; h++)
    {
        error = FALSE;
        j = fpmatch (start);
        if (!error)
        {
            size = disassembleattempt (start, j);
            if (size != 0)
                break;
        }
    }
#endif
/*    if (size == 0)
    {
        printf ("\n%c%8x", HEXDEL, start);
        printf (" %4X\t\t  DC.W", get16 (start));
        printf (" \t\t?  ");
        return (2);
    }
    else
        return (size);*/

    return 0;
}

/* ******************************************************************** *
 * MovBytes() - Reads data for Data Boundary range from input file and  *
 *          places it onto the print buffer (and does any applicable    *
 *          printing if in pass 2).                                     *
 * ******************************************************************** */

void
#ifdef __STDC__
MovBytes (register struct databndaries *db)
#else
MovBytes (db)
    register struct databndaries *db;
#endif
{
    CMD_ITMS Ci;
    register CMD_ITMS *cmd_pt = &Ci;
    char tmps[20];
    register unsigned int valu;
    register int bmask;
#ifdef _OSK
    static
#endif
    /*char *xFmt[3] = {"$%02x", "$%04x", "$%08x"};*/
    char xtrabytes[50];
    register int cCount = 0,
        maxLst;
    register char *xtrafmt;

    CmdEnt = PCPos;

    /* This may be temporary, and we may set PBytSiz
     * to the appropriate value */
    *xtrabytes = '\0';
    strcpy (cmd_pt->mnem, "dc");
    cmd_pt->opcode[0] = '\0';
    cmd_pt->lblname = "";
    cmd_pt->comment = NULL;
    cmd_pt->cmd_wrd = 0;
    PBytSiz = db->b_siz;

    if (PBytSiz < 4)
    {
        xtrafmt = "%02x";
    }
    else
    {
        xtrafmt = "%04x";
    }

    switch (PBytSiz)
    {
    case 1:
        strcat (cmd_pt->mnem, ".b");
        maxLst = 4;
        bmask = 0xff;
        break;
    case 2:
        strcat (cmd_pt->mnem, ".w");
        maxLst = 2;
        bmask = 0xffff;
        break;
    case 4:
        strcat (cmd_pt->mnem, ".l");
        maxLst = 1;
        bmask = 0xffff;
        break;
    }

    while (PCPos <= db->b_hi)
    {
        /* Init dest buffer to null string for LblCalc concatenation */

        tmps[0] = '\0';

        switch (PBytSiz)
        {
        case 1:
            valu = fread_b(ModFP);
            break;
        case 2:
            valu = fread_w (ModFP);
            break;
        case 4:
            valu = fread_l (ModFP);
            break;
        }

        PCPos += db->b_siz;
        ++cCount;

        /*process_label (cmd_pt, AMode, valu);*/


        LblCalc (tmps, valu, AMode, PCPos - db->b_siz);

        if (Pass == 2)
        {
            char tmpval[10];

            if (cCount == 0)
            {
                cmd_pt->cmd_wrd = valu & bmask;
            }
            else if (cCount < maxLst)
            {
                cmd_pt->cmd_wrd =  ((cmd_pt->cmd_wrd << (PBytSiz * 8)) | (valu & bmask));
            }
            else
            {
                sprintf (tmpval, xtrafmt, valu & bmask);
                strcat (xtrabytes, tmpval);
            }

            ++cCount;

            if (strlen(cmd_pt->opcode))
            {
                strcat (cmd_pt->opcode, ",");
            }

            strcat (cmd_pt->opcode, tmps);

            /* If length of operand string is max, print a line */

            if ( (strlen (cmd_pt->opcode) > 22) || findlbl ('L', PCPos))
            {
                PrintLine(pseudcmd, cmd_pt, 'L', CmdEnt, PCPos);

                if (strlen(xtrabytes))
                {
                    printXtraBytes (xtrabytes);
                }

                cmd_pt->opcode[0] = '\0';
                cmd_pt->cmd_wrd = 0;
                cmd_pt->lblname = NULL;
                CmdEnt = PCPos/* _ PBytSiz*/;
                cCount = 0;
            }
        }

        /*PCPos += PBytSiz;*/
    }

    /* Loop finished.. print any unprinted data */

    if ((Pass == 2) && strlen (cmd_pt->opcode))
    {
        PrintLine (pseudcmd, cmd_pt, 'L', CmdEnt, PCPos);

        if (strlen(xtrabytes))
        {
            printXtraBytes (xtrabytes);
        }
    }
}


/* ******************************************
 * MovAsc() - Move nb byes fordcb" statement
 */

void
#ifdef __STDC__
MovASC (register int nb, register char aclass)
#else
MovASC (nb, aclass)
    register int nb;
    register char aclass;
#endif
{
    char oper_tmp[30];
    CMD_ITMS Ci;
    register CMD_ITMS *cmd_pt = &Ci;
    register int cCount = 0;

    strcpy (cmd_pt->mnem, "dc.b");         /* Default mnemonic to "fcc" */
    CmdEnt = PCPos;
    *oper_tmp = '\0';
    cmd_pt->cmd_wrd = 0;
    cmd_pt->comment = NULL;
    cmd_pt->lblname = "";

    while (nb--)
    {
        register int x;
        char c[6];

        if ((strlen (oper_tmp) > 24) ||
            (strlen (oper_tmp) && findlbl (aclass, PCPos)))
            /*(strlen (oper_tmp) && findlbl (ListRoot (aclass), PCPos + 1)))*/
        {
            sprintf (cmd_pt->opcode, "\"%s\"", oper_tmp);
            PrintLine (pseudcmd, cmd_pt, 'L', CmdEnt, PCPos);
            oper_tmp[0] = '\0';
            CmdEnt = PCPos;
            cmd_pt->lblname = "";
            cmd_pt->cmd_wrd = 0;
            cmd_pt->wcount = 0;
            cCount = 0;
        }

        x = fgetc (ModFP);
        ++cCount;
        ++PCPos;

        if (isprint (x) && (x != '"'))
        {
            if (Pass == 2)
            {
                /*if (strlen (pbuf->instr) < 12)
                {
                    sprintf (c, "%02x ", x);
                    strcat (pbuf->instr, c);
                }*/

                sprintf (c, "%c", x & 0x7f);
                strcat (oper_tmp, c);

                if (cCount < 16)
                {
                    cmd_pt->cmd_wrd = (cmd_pt->cmd_wrd << 8) | (x & 0xff);
                }
            }   /* end if (Pass2) */
        }
        else            /* then it's a control character */
        {
            if (Pass == 1)
            {
                if ((x & 0x7f) < 33)
                {
                    char lbl[10];
                    sprintf (lbl, "ASC%02x", x);
                    addlbl ('^', x & 0xff, lbl);
                }
            }
            else       /* Pass 2 */
            {
                /* Print any unprinted ASCII characters */
                if (strlen (oper_tmp))
                {
                    sprintf (cmd_pt->opcode, "\"%s\"", oper_tmp);
                    PrintLine (pseudcmd, cmd_pt, aclass, CmdEnt, CmdEnt);
                    cmd_pt->opcode[0] = '\0';
                    cmd_pt->cmd_wrd = 0;
                    cmd_pt->lblname = "";
                    oper_tmp[0] = '\0';
                    cCount = 0;
                    CmdEnt = PCPos - 1; /* We already have the byte */
                }

                if (isprint(x))
                {
                    sprintf (cmd_pt->opcode, "'%c'", x);
                }
                else
                {
                    sprintf (cmd_pt->opcode, "$%x", x);
                }

                cmd_pt->cmd_wrd = x;
                PrintLine (pseudcmd, cmd_pt, aclass, CmdEnt, PCPos);
                cmd_pt->lblname = "";
                cmd_pt->opcode[0] = '\0';
                cmd_pt->cmd_wrd = 0;
                cCount = 0;
                CmdEnt = PCPos;
            }
        }
    }       /* end while (nb--) - all chars moved */

    /* Finally clean up any remaining data. */
    if ((Pass == 2) && (strlen (oper_tmp)) )       /* Clear out any pending string */
    {
        sprintf (cmd_pt->opcode, "\"%s\"", oper_tmp);
        /*list_print (cmd_pt, CmdEnt, NULL);*/
        PrintLine (pseudcmd, cmd_pt, 'L', CmdEnt, PCPos);
        cmd_pt->lblname = "";
        *oper_tmp = '\0';
    }

    CmdEnt = PCPos;
}

/* ********************************* *
 * NsertBnds():	Insert boundary area *
 * ********************************* */

void
#ifdef __STDC__
NsrtBnds (register struct databndaries *bp)
#else
NsrtBnds (bp)
    register struct databndaries *bp;
#endif
{
    /*memset (pbuf, 0, sizeof (struct printbuf));*/
    AMode = 0;                  /* To prevent LblCalc from defining class */
    NowClass = bp->b_class;
    PBytSiz = 1;                /* Default to one byte length */

    switch (bp->b_typ)
    {
        case 1:                    /* Ascii */
            /* Bugfix?  Pc was bp->b_lo...  that setup allowed going past
             * the end if the lower bound was not right. */

            MovASC ((bp->b_hi) - PCPos + 1, 'L');
            break;                  /* bump PC  */
        case 7:                     /* Word */
            PBytSiz = 2;            /* Takes care of both Word & Long */
            MovBytes(bp);
            break;
        case 5:                     /* Long */
            PBytSiz = 4;            /* Takes care of both Word & Long */
            MovBytes(bp);
            break;
        case 2:                     /* Byte */
        case 6:                     /* Short */
            MovBytes (bp);
            break;
        case 3:                    /* "C"ode .. not implememted yet */
            break;
        default:
            break;
    }

    NowClass = 0;
}

#ifdef DoIsCmd
/* ******************************************************************** *
 * IsCmd() - Checks to see if code pointed to by p is valid code.       *
 *      On entry, we are poised at the first byte of prospective code.  *
 * Returns: pointer to valid lkuptable entry or 0 on fail               *
 * ******************************************************************** */

static struct lkuptbl *
IsCmd (register int *fbyte, register int *csiz)
{
    register struct lkuptbl *T;          /* pointer to appropriate tbl   */
    register int sz;            /* # entries in this table      */
    register int c = fgetc (progpath);

    *csiz = 2;

    switch (*fbyte = c)
    {
        case '\x10':
            T = Pre10;
            c = fgetc (progpath);
            *fbyte =(*fbyte <<8) + c;
            sz = sizeof (Pre10) / sizeof (Pre10[0]);
            break;
        case '\x11':
            T = Pre11;
            c = fgetc (progpath);
            *fbyte =(*fbyte <<8) + c;
            sz = sizeof (Pre11) / sizeof (Pre11[0]);
            break;
        default:
            *csiz = 1;
            T = Byte1;
            sz = sizeof (Byte1) / sizeof (struct lkuptbl);
            break;
    }

    while ((T->cod != c))
    {
        if (--sz == 0)
        {
            return 0;
        }

        ++T;
    }

    AMode = T->amode;

    return ((T->cod == c) && (T->t_cpu <= CpuTyp)) ? T : 0;
}

#endif           /* ifdef IsCmd */

