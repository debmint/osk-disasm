/* ************************************************************** *
 * proto_h - Prototypes for functions in dis68                    $
 *                                                                $
 * This file handles both STDC and non-STDC forms                 $
 * $Id::                                                          $
 * ************************************************************** */

#ifndef _HAVEPROTO_
#define _HAVEPROTO_
#ifdef __STDC__
/* cmdfile.c */
void do_cmd_file(void);
int ApndCmnt(register char *lpos);
char *cmntsetup(register char *cpos, register char *clas, register int *adrs);
char *cmdsplit(register char *dest, register char *src);
void getrange(register char *pt, register int *lo, register int *hi, register int usize, register int allowopen);
void boundsline(register char *mypos);
void setupbounds(register char *lpos);
void tellme(register char *pt);
/* commonsubs.c */
int get_eff_addr(register CMD_ITMS *ci, register char *ea, register u_char mode, register u_char reg, register u_char size);
char *pass_eq(register char *p);
int strpos(register char *s, register char c);
char *skipblank(register char *p);
int get_extends_common(register CMD_ITMS *ci, register char *mnem);
int reg_ea(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
char *regbyte(register char *s, register unsigned char regmask, register char *ad, register int doslash);
int revbits(register int num, register int lgth);
int movem_cmd(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int link_unlk(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
unsigned int fget_w(register FILE *fp);
unsigned int fget_l(register FILE *fp);
char fread_b(register FILE *fp);
char getnext_b(register CMD_ITMS *ci);
short getnext_w(register CMD_ITMS *ci);
void ungetnext_w(register CMD_ITMS *ci);
short fread_w(register FILE *fp);
int fread_l(register FILE *fp);
short bufReadW(register char **pt);
int bufReadL(register char **pt);
void *mem_alloc(register int req);
char *freadString(void);
char *lbldup(register char *lbl);
/* def68def.c */
OPSTRUCTURE *tablematch(register int opword, register OPSTRUCTURE *entry);
/* dis68.c */
int main(register int argc, register char **argv);
FILE *build_path(register char *p, register char *faccs);
void do_opt(register char *c);
void errexit(register char *pmpt);
void filereadexit(void);
/* dismain.c */
struct modnam *modnam_find(register struct modnam *pt, register int desired);
int dopass(void);
int showem(void);
int notimplemented(register CMD_ITMS *ci, register int tblno, register OPSTRUCTURE *op);
void MovBytes(register struct databndaries *db);
void MovASC(register int nb, register char aclass);
void NsrtBnds(register struct databndaries *bp);
/* dprint.c */
void PrintAllCodLine(register u_short w1, register u_short w2);
void PrintAllCodL1(register u_short w1);
void PrintPsect(void);
char *get_apcomment(char clas, register int addr);
void PrintLine(register char *pfmt, register CMD_ITMS *ci, register char cClass, register int cmdlow, int cmdhi);
void printXtraBytes(register char *data);
void PrintComment(char lblcClass, register int cmdlow, register int cmdhi);
void ROFPsect(register struct rof_hdr *rptr);
void WrtEnds(void);
void ParseIRefs(char rClass);
void GetIRefs(void);
int DoAsciiBlock(register CMD_ITMS *ci, register char *buf, register int bufEnd, char iClass);
void ROFDataPrint(void);
char *LoadIData(void);
void OS9DataPrint(void);
void ListData(register LBLDEF *me, register int upadr, char cClass);
void WrtEquates(register int stdflg);
/* lblfuncs.c */
LBLCLAS *labelclass(register char lblclass);
void PrintLbl(register char *dest, register u_char clas, register int adr, register LBLDEF *dl, register u_char amod);
struct databndaries *ClasHere(register struct databndaries *bp, register int adrs);
LBLDEF *findlbl(register char lblclass, register int lblval);
char *lblstr(register char lblclass, register int lblval);
LBLDEF *addlbl(register char lblclass, register int val, register char *newname);
void process_label(register CMD_ITMS *ci, register char lblclass, register int addr);
void parsetree(register char c);
int LblCalc(register char *dst, register int adr, register u_char amod, register int curloc);
/* opcode00.c */
int biti_reg(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int biti_size(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int bit_static(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int bit_dynamic(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int move_instr(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int move_ccr_sr(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int move_usp(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int movep(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int moveq(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int one_ea(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int bra_bsr(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_no_opcode(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int bit_rotate_mem(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int bit_rotate_reg(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int br_cond(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int add_sub(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmp_cmpa(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int addq_subq(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int abcd_sbcd(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
void addTrapOpt(register CMD_ITMS *ci, register int ppos);
int trap(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_stop(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_dbcc(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_scc(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_exg(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int ext_extb(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmpm_addx_subx(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
/* opcodes020.c */
int cmp2_chk2(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int rtm_020(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_moves(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_cas(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_cas2(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_callm(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int muldiv_020(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_rtd(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int cmd_trapcc(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
int bitfields_020(register CMD_ITMS *ci, register int j, register OPSTRUCTURE *op);
/* rof.c */
void reflst(void);
int RealEnt(void);
void AddInitLbls(register struct rof_extrn *tbl, register int dataSiz, register char klas);
void getRofHdr(register FILE *progpath);
void RofLoadInitData(void);
char rof_class(register int typ, register int refTy);
struct rof_extrn *find_extrn(register struct rof_extrn *xtrn, register int adrs);
int typeFetchSize(register int rtype);
struct rof_extrn *rof_lblref(register CMD_ITMS *ci, register int *value);
int rof_datasize(register char cclass);
void ListInitROF(register char *hdr, struct rof_extrn *refsList, register char *iBuf, register int isize, register char iClass);
void rof_ascii(register char *cmdline);
void setROFPass(void);
int rof_setup_ref(register struct rof_extrn *ref, register int addrs, register char *dest, register int val);
char *IsRef(register char *dst, register int curloc, register int ival);

#else

/* cmdfile.c */
void do_cmd_file();
int ApndCmnt();
char *cmntsetup();
char *cmdsplit();
void getrange();
void boundsline();
void setupbounds();
void tellme();
/* commonsubs.c */
int get_eff_addr();
char *pass_eq();
int strpos();
char *skipblank();
int get_extends_common();
int reg_ea();
char *regbyte();
int revbits();
int movem_cmd();
int link_unlk();
unsigned int fget_w();
unsigned int fget_l();
char fread_b();
char getnext_b();
short getnext_w();
void ungetnext_w();
short fread_w();
int fread_l();
short bufReadW();
int bufReadL();
void *mem_alloc();
char *freadString();
char *lbldup();
/* def68def.c */
OPSTRUCTURE *tablematch();
/* dis68.c */
int main();
FILE *build_path();
void do_opt();
void errexit();
void filereadexit();
/* dismain.c */
struct modnam *modnam_find();
int dopass();
int showem();
int notimplemented();
void MovBytes();
void MovASC();
void NsrtBnds();
/* dprint.c */
void PrintAllCodLine();
void PrintAllCodL1();
void PrintPsect();
char *get_apcomment();
void PrintLine();
void printXtraBytes();
void PrintComment();
void ROFPsect();
void WrtEnds();
void ParseIRefs();
void GetIRefs();
int DoAsciiBlock();
void ROFDataPrint();
char *LoadIData();
void OS9DataPrint();
void ListData();
void WrtEquates();
/* lblfuncs.c */
LBLCLAS *labelclass();
void PrintLbl();
struct databndaries *ClasHere();
LBLDEF *findlbl();
char *lblstr();
LBLDEF *addlbl();
void process_label();
void parsetree();
int LblCalc();
/* opcode00.c */
int biti_reg();
int biti_size();
int bit_static();
int bit_dynamic();
int move_instr();
int move_ccr_sr();
int move_usp();
int movep();
int moveq();
int one_ea();
int bra_bsr();
int cmd_no_opcode();
int bit_rotate_mem();
int bit_rotate_reg();
int br_cond();
int add_sub();
int cmp_cmpa();
int addq_subq();
int abcd_sbcd();
void addTrapOpt();
int trap();
int cmd_stop();
int cmd_dbcc();
int cmd_scc();
int cmd_exg();
int ext_extb();
int cmpm_addx_subx();
/* opcodes020.c */
int cmp2_chk2();
int rtm_020();
int cmd_moves();
int cmd_cas();
int cmd_cas2();
int cmd_callm();
int muldiv_020();
int cmd_rtd();
int cmd_trapcc();
int bitfields_020();
/* rof.c */
void reflst();
int RealEnt();
void AddInitLbls();
void getRofHdr();
void RofLoadInitData();
char rof_class();
struct rof_extrn *find_extrn();
int typeFetchSize();
struct rof_extrn *rof_lblref();
int rof_datasize();
void ListInitROF();
void rof_ascii();
void setROFPass();
int rof_setup_ref();
char *IsRef();
#endif   /* __STDC__*/

#endif   /* #ifndef _HAVEPROTO_*/

